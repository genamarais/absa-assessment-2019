CREATE TABLE swift_message (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  transaction_ref VARCHAR(16) NOT NULL,
  client_swift_address VARCHAR(11) NOT NULL,
  message_status VARCHAR(9) NOT NULL,
  currency VARCHAR(3) NOT NULL,
  amount DECIMAL NOT NULL,
  date_time_created TIMESTAMP NOT NULL
);

CREATE TABLE usage_data (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  client_swift_address VARCHAR(11) NOT NULL,
  subservice VARCHAR(6) NOT NULL,
  date DATE NOT NULL ,
  usage_stats NUMERIC(6) NOT NULL,
  UNIQUE (client_swift_address,subservice, date)
);