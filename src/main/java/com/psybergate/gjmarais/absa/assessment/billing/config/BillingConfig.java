package com.psybergate.gjmarais.absa.assessment.billing.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.psybergate.gjmarais.absa.assessment.billing")
@EnableConfigurationProperties(FileStorageProperties.class)
@PropertySource({"classpath:billing-application.properties"})
public class BillingConfig {
}
