package com.psybergate.gjmarais.absa.assessment.host.job;

import com.psybergate.gjmarais.absa.assessment.host.domain.UsageDataRecord;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileCreator;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class UsageDataTextFileCreator implements FileCreator<UsageDataRecord> {

    @Override
    public String getFileName() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".txt";
    }

    @Override
    public byte[] getFileContentAsBytes(List<UsageDataRecord> unformattedRecords) {
        List<String> formattedRecords = unformattedRecords.stream()
                .map(m -> format(m))
                .collect(Collectors.toList());

        String fileContents = String.join("\n", formattedRecords);

        return fileContents.getBytes();
    }

    public String format(UsageDataRecord usageDataRecord) {
        return String.format("%17s%11s%6s%8s%6s",
                "INTEGRATEDSERVICES", usageDataRecord.getClientSwiftAddress(), usageDataRecord.getSubService(), usageDataRecord.getDate().format(DateTimeFormatter.ofPattern("yyyyMMdd")), usageDataRecord.getUsageStats());
    }

}
