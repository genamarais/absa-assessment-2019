package com.psybergate.gjmarais.absa.assessment.host.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain = true)
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@Table(name ="swift_message")
public class SwiftMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="transaction_ref", nullable = false)
    private String transactionReference;

    @Column(name="client_swift_address", length=11, nullable = false)
    private String clientSwiftAddress;

    @Enumerated(EnumType.STRING)
    @Column(name="message_status", nullable = false)
    private MessageStatus messageStatus;

    @Enumerated(EnumType.STRING)
    @Column(name="currency", nullable = false)
    private Currency currency;

    @Column(name="amount", nullable = false)
    private BigDecimal amount;

    @Column(name="date_time_created", nullable = false)
    private LocalDateTime dateTimeCreated;
}
