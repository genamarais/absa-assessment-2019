package com.psybergate.gjmarais.absa.assessment.host.job;

import com.psybergate.gjmarais.absa.assessment.host.domain.UsageDataRecord;
import com.psybergate.gjmarais.absa.assessment.host.repository.UsageDataRepository;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileCreator;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileUploadService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class UsageDataJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(UsageDataJob.class);

    private final UsageDataRepository usageDataRepository;

    private final FileCreator fileCreator;

    private final FileUploadService fileUploadService;

    private final String uploadUrl;

    @Autowired
    public UsageDataJob(UsageDataRepository usageDataRepository, @Qualifier("usageDataFileCreator") FileCreator fileCreator, FileUploadService fileUploadService, @Value("${billing.usage-data.upload.url}") String uploadUrl) {
        this.usageDataRepository = usageDataRepository;
        this.fileCreator = fileCreator;
        this.fileUploadService = fileUploadService;
        this.uploadUrl = uploadUrl;
    }

    @Override
    @Transactional
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            Date startDate = startDate(jobExecutionContext);
            Date endDate = jobExecutionContext.getFireTime();
            List<UsageDataRecord> allUsageDatumRecords = usageDataRepository.extractUsageData(startDate, endDate);
            usageDataRepository.saveAll(allUsageDatumRecords);

            String fileName = fileCreator.getFileName();
            byte[] fileContent = fileCreator.getFileContentAsBytes(allUsageDatumRecords);
            ResponseEntity<String> response = fileUploadService.postFile(fileName, fileContent, uploadUrl);
            logFileUploadResponse(fileName, response);

        } catch (Exception e) {
            logger.error("Unable to submit usage data.");
        }

    }

    private void logFileUploadResponse(String fileName, ResponseEntity<String> response) {
        logger.info("Usage data file " + fileName + " submitted to " + uploadUrl);
        String log = String.format("%s response - %s", uploadUrl, response.getStatusCode().toString());
        if (response.getStatusCode().is2xxSuccessful()) {
            logger.info(log);
        } else {
            logger.error(log);
        }
    }

    public Date startDate(JobExecutionContext jobExecutionContext) {

        Calendar myCal = Calendar.getInstance();
        myCal.set(Calendar.YEAR, 2019);
        myCal.set(Calendar.MONTH, 11);
        myCal.set(Calendar.DAY_OF_MONTH, 01);
        Date startDate = myCal.getTime();

//        Date startDate = jobExecutionContext.getPreviousFireTime();
//        if(startDate!= null) return startDate;
//
//        Date fireTime = jobExecutionContext.getFireTime();
//        Date nextFireTime = jobExecutionContext.getNextFireTime();
//
//        long interval = nextFireTime.getTime() - fireTime.getTime();
//        long previous = fireTime.getTime() - interval;
//        startDate = new Date();
//        startDate.setTime(previous);
        return startDate;
    }

}
