package com.psybergate.gjmarais.absa.assessment.host.service.fileupload;

import org.springframework.http.ResponseEntity;

public interface FileUploadService {
    ResponseEntity<String> postFile(String filename, byte[] fileContent, String url);
}
