package com.psybergate.gjmarais.absa.assessment.host.repository;

import com.psybergate.gjmarais.absa.assessment.host.domain.UsageDataRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UsageDataRepository extends JpaRepository<UsageDataRecord, Long> {

    @Query(nativeQuery = true)
    List<UsageDataRecord> extractUsageData(@Param("startdate") Date startDate, @Param("enddate") Date endDate);
}
