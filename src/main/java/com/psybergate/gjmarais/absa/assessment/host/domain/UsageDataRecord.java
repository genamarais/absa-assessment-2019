package com.psybergate.gjmarais.absa.assessment.host.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Entity
@Data
@Accessors(chain = true)
@EqualsAndHashCode
@NoArgsConstructor
@Table(name="usage_data",
        uniqueConstraints= @UniqueConstraint(columnNames={"client_swift_address", "subservice", "date"}))
public class UsageDataRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "client_swift_address", length=12, nullable = false)
    private String clientSwiftAddress;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name= "subservice", length = 6, nullable = false)
    private SubService subService;

    @Column(name = "usage_stats", length=7, nullable = false)
    private int usageStats;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;


    public UsageDataRecord(@NotBlank String clientSwiftAddress, @NotBlank String subService, @NotNull LocalDate date, int usageStats) {
        this.clientSwiftAddress = clientSwiftAddress;
        this.subService = SubService.valueOf(subService);
        this.date = date;
        this.usageStats = usageStats;
    }

}
