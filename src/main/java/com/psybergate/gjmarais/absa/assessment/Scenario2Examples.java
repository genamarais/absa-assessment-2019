package com.psybergate.gjmarais.absa.assessment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Scenario2Examples {

    public static void main(String[] args) {
        List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity", "Completed");

        System.out.println(defaultStreamFiltering(statuses));

        System.out.println(parallelStreamFiltering(statuses));

        System.out.println(sequentialStreamFiltering(statuses));
    }

    public static List<String> defaultStreamFiltering(List<String> unfilteredList){
        return unfilteredList
                    .stream()
                    .filter(s -> !s.isEmpty())
                    .collect(Collectors.toList());
    }

    public static List<String> parallelStreamFiltering(List<String> unfilteredList){
        return unfilteredList
                .stream()
                .parallel()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

    public static List<String> sequentialStreamFiltering(List<String> unfilteredList){
        return unfilteredList
                .stream()
                .sequential()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }


}
