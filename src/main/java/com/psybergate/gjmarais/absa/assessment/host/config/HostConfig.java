package com.psybergate.gjmarais.absa.assessment.host.config;

import com.psybergate.gjmarais.absa.assessment.host.job.UsageDataJob;
import com.psybergate.gjmarais.absa.assessment.host.job.UsageDataTextFileCreator;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileCreator;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;


@Configuration
@EnableScheduling
@EnableTransactionManagement
@EnableJpaRepositories("com.psybergate.gjmarais.absa.assessment.host.repository")
@EnableJpaAuditing
@ComponentScan("com.psybergate.gjmarais.absa.assessment.host")
@PropertySource({"classpath:host-application.properties"})
public class HostConfig implements EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public FileCreator usageDataFileCreator (){
        return new UsageDataTextFileCreator();
    }

    @Bean
    public JobDetailFactoryBean jobDetail() {
        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(UsageDataJob.class);
        jobDetailFactory.setDescription("Invoke Sample Usage Data Job service...");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }

    @Bean
    public Trigger trigger(JobDetail job) {
        String cron = environment.getProperty("billing.usage-data.cron");
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("Qrtz_Usage_Data_Trigger")
                .withDescription("Usage data job trigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                .build();
    }



}
