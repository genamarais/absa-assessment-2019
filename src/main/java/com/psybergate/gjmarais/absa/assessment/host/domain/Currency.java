package com.psybergate.gjmarais.absa.assessment.host.domain;

public enum Currency {
    ZAR, USD, EUR
}
