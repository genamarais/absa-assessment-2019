package com.psybergate.gjmarais.absa.assessment.host.service.fileupload;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface FileCreator<T> {

    public String getFileName();

    public byte[] getFileContentAsBytes(List<T> records);

}
