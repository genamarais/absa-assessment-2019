package com.psybergate.gjmarais.absa.assessment.host.domain;

public enum MessageStatus {

    COMPLETED, REJECTED
}
