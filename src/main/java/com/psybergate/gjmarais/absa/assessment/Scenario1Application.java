package com.psybergate.gjmarais.absa.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class Scenario1Application {

	public static void main(String[] args) {
		SpringApplication.run(Scenario1Application.class, args);
	}

}
