package com.psybergate.gjmarais.absa.assessment.host.job;

import com.psybergate.gjmarais.absa.assessment.host.domain.UsageDataRecord;
import com.psybergate.gjmarais.absa.assessment.host.repository.UsageDataRepository;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileCreator;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileUploadService;
import org.junit.jupiter.api.Test;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UsageDataJobTest {

    @Test
    public void verifyServicesBeingCalled() {

        Date startDate = null;
        Date endDate = null;

        LocalDate extractDate = LocalDate.now();
        UsageDataRecord usageDataRecordRecord1 = new UsageDataRecord("ABSAZAJJXXX", "CCYOUT", extractDate, 7);
        UsageDataRecord usageDataRecordRecord2 = new UsageDataRecord("ABSAZAJJXXX", "ZAROUT", extractDate, 5);
        List<UsageDataRecord> usageDataRecordRecords = Arrays.asList(usageDataRecordRecord1, usageDataRecordRecord2);

        UsageDataRepository usageDataRepository = mock(UsageDataRepository.class);
        when(usageDataRepository.extractUsageData(any(Date.class), any(Date.class))).thenReturn(usageDataRecordRecords);

        String uploadUrl = "locahost:8080/foo";
        String fileName = "usageData.txt";
        byte[] fileContents = "usageDataFormattedFileContents".getBytes();

        FileCreator fileCreator = mock(FileCreator.class);
        when(fileCreator.getFileName()).thenReturn(fileName);
        when(fileCreator.getFileContentAsBytes(any(List.class))).thenReturn(fileContents);

        ResponseEntity response = mock(ResponseEntity.class);
        when(response.getStatusCode()).thenReturn(HttpStatus.OK);

        FileUploadService fileUploadService = mock(FileUploadService.class);
        when(fileUploadService.postFile(fileName, fileContents, uploadUrl)).thenReturn(response);

        JobExecutionContext jobExecutionContext = mock(JobExecutionContext.class);

        UsageDataJob usageDataJob = spy(new UsageDataJob(usageDataRepository, fileCreator, fileUploadService, uploadUrl));
        when(usageDataJob.startDate(jobExecutionContext)).thenReturn(startDate);

        try {
            usageDataJob.execute(jobExecutionContext);

            verify(usageDataRepository).extractUsageData(startDate, endDate);
            verify(fileCreator).getFileName();
            verify(fileCreator).getFileContentAsBytes(any(List.class));
            verify(fileUploadService).postFile(fileName, fileContents, uploadUrl);
//            verify(usageDataRepository).saveAll(usageDataRecordRecords);

        } catch (JobExecutionException e) {
            throw new RuntimeException(e);
        }

    }

}