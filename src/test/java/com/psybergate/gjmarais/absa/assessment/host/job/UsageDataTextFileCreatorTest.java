package com.psybergate.gjmarais.absa.assessment.host.job;

import com.psybergate.gjmarais.absa.assessment.host.domain.UsageDataRecord;
import com.psybergate.gjmarais.absa.assessment.host.service.fileupload.FileCreator;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UsageDataTextFileCreatorTest {

    private final FileCreator fileCreator = new UsageDataTextFileCreator();

    @Test
    public void shouldReturnCurrentDateTxt_WhenRequestingFileName(){
        String expected = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".txt";
        String actual = fileCreator.getFileName();
        assertEquals(expected,actual);
    }

    @Test
    public void shouldReturnCorrectFileContents_WhenGivenUsageData(){
        LocalDate extractDate = LocalDate.now();
        String date = extractDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        UsageDataRecord usageDataRecord1 = new UsageDataRecord("ABSAZAJJXXX", "CCYOUT", extractDate, 7);
        UsageDataRecord usageDataRecord2 = new UsageDataRecord("ABSAZAJJXXX", "ZAROUT", extractDate, 2);

        List<UsageDataRecord> usageData = Arrays.asList(usageDataRecord1, usageDataRecord2);
        String fileContentsString = String.format("INTEGRATEDSERVICESABSAZAJJXXXCCYOUT%s     7\nINTEGRATEDSERVICESABSAZAJJXXXZAROUT%s     2", date, date);

        byte[] expected = fileContentsString.getBytes();
        byte[] actual = fileCreator.getFileContentAsBytes(usageData);
        assertArrayEquals(expected,actual);
    }

    @Test
    public void shouldCorrectlyFormatUsageData_GivenUsageDataRecord(){
        UsageDataTextFileCreator usageDataTextFileCreator = (UsageDataTextFileCreator) fileCreator;
        LocalDate extractDate = LocalDate.now();
        UsageDataRecord usageDataRecord = new UsageDataRecord("ABSAZAJJXXX", "CCYOUT", extractDate, 7);

        String expected = "INTEGRATEDSERVICESABSAZAJJXXXCCYOUT" + extractDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "     7";
        String actual = usageDataTextFileCreator.format(usageDataRecord);
        assertEquals(expected,actual);
    }

}